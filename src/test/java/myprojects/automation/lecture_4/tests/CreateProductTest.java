package myprojects.automation.lecture_4.tests;

import myprojects.automation.lecture_4.BaseTest;
import myprojects.automation.lecture_4.model.AdminData;
import myprojects.automation.lecture_4.model.ProductData;
import myprojects.automation.lecture_4.utils.Properties;
import myprojects.automation.lecture_4.utils.logging.CustomReporter;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    @Test(dataProvider = "credentials", dataProviderClass = AdminData.class)
    public void createNewProduct(String login, String password) {
        CustomReporter.log("Open Admin panel");
        actions.open(Properties.getBaseAdminUrl());
        CustomReporter.log("Login into Admin panel");
        actions.login(login, password);
        CustomReporter.log("Create product");
        CustomReporter.log("The product is " + ProductData.getInstance().getName());
        actions.createProduct(ProductData.getInstance());
    }

    @Test (dependsOnMethods = "createNewProduct")
    public void verifyProductWasAdded() {
        CustomReporter.log("Open Main page of the shop");
        actions.open(Properties.getBaseUrl());
        actions.verifyProductExists();
    }
}