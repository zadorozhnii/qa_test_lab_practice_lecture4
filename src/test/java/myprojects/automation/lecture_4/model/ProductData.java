package myprojects.automation.lecture_4.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

/**
 * Hold Product information that is used among tests.
 */
public class ProductData {
    private static ProductData productDataInstance = null;
    private String name;
    private int qty;
    private float price;

    private ProductData(String name, int qty, float price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public static ProductData getInstance(){
        if(productDataInstance == null){
            productDataInstance = generate();
        }
        return productDataInstance;
    }

    public String getName() {
        return name;
    }

    public Integer getQty() {
        return qty;
    }

    public String getPrice() {
        DecimalFormatSymbols separators = new DecimalFormatSymbols();
        separators.setDecimalSeparator(',');
        return new DecimalFormat("#0.00", separators).format(price);
    }

    /**
     * @return New Product object with random name, quantity and price values.
     */
    public static ProductData generate() {
        Random random = new Random();
        return new ProductData(
                "New Product " + System.currentTimeMillis(),
                random.nextInt(100) + 1,
//                (float) Math.round(random.nextInt(100_00) + 1) / 100);
        (float) Math.round(random.nextInt(100_00) + 1) / 100);
    }
}
