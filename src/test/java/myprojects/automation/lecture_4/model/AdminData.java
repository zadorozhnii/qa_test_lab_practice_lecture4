package myprojects.automation.lecture_4.model;

import org.testng.annotations.DataProvider;

public class AdminData {
    private static final String LOGIN = "webinar.test@gmail.com";
    private static final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";

    @DataProvider
    private static Object[][] credentials(){
        return new Object[][]{{getLogin(), getPassword()}};
    }

    /**
     *
     * @return The password of the admin console.
     */
    public static String getPassword() {
        return PASSWORD;
    }

    /**
     *
     * @return The login of the admin console.
     */
    public static String getLogin() {
        return LOGIN;
    }
}
