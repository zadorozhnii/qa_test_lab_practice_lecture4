package myprojects.automation.lecture_4;


import myprojects.automation.lecture_4.model.ProductData;
import myprojects.automation.lecture_4.utils.Properties;
import myprojects.automation.lecture_4.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static org.testng.Assert.assertFalse;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private static By loginEmailSelector = By.id("email");
    private static By passwordSelector = By.id("passwd");
    private static By ajaxIconSelector = By.id("ajax_running");
    private static By submitButtonSelector = By.name("submitLogin");
    private static By catalogItemSelector = By.id("subtab-AdminCatalog");
    private static By productSelector = By.id("subtab-AdminProducts");
    private static By addProductButtonSelector = By.id("page-header-desc-configuration-add");
    private static By productNameInputSelector = By.id("form_step1_name_1");
    private static By productQuantityInputSelector = By.id("form_step1_qty_0_shortcut");
    private static By productPriceInputSelector = By.id("form_step1_price_shortcut");
    private static By activateSwitchSelector = By.cssSelector(".switch-input");
    private static By successMessageSelector = By.cssSelector(".growl.growl-notice.growl-medium");
    private static By saveButtonSelector = By.id("submit");
    private static By closeSuccessMessageSelector = By.cssSelector(".growl-close");
    private static By allProductsSelector = By.cssSelector(".all-product-link.pull-xs-left.pull-md-right.h4");
    private static By createdProductSelector = By.xpath("//a[contains(.,'" + ProductData.getInstance().getName() +"')]");
    private static By createdProductNameSelector = By.cssSelector(".h1");
    private static By createdProductPriceSelector = By.xpath("//div[@class = 'current-price']/span");
    private static By createdProductQtySelector = By.xpath("//div[@class = 'product-quantities']/span");
    private static By searchFieldSelector = By.cssSelector(".ui-autocomplete-input");
    private static By searchButtonSelector = By.cssSelector(".material-icons.search");


    public GeneralActions(WebDriver driver) {
            this.driver = driver;
            wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
            driver.findElement(loginEmailSelector).sendKeys(login);
            driver.findElement(passwordSelector).sendKeys(password);
            driver.findElement(submitButtonSelector).click();
            waitForElementClickable(catalogItemSelector);
    }

    /**
     * Creates a new product.
     * @param newProduct
     */
    public void createProduct(ProductData newProduct) {
            waitForElementClickable(catalogItemSelector);
            Actions action = new Actions(driver);
            WebElement catalog = driver.findElement(catalogItemSelector);
            WebElement adminProduct = driver.findElement(productSelector);
            action.moveToElement(catalog).build().perform();
            waitForElementClickable(productSelector);
            action.moveToElement(adminProduct).click().build().perform();
            waitForElementClickable(addProductButtonSelector);
            driver.findElement(addProductButtonSelector).click();
            waitForElementClickable(productNameInputSelector);
            fillInProductData(newProduct);
            driver.findElement(saveButtonSelector).click();
            checkSuccessMessage();
    }

    /**
     * Verify the new product exists.
     */
    public void verifyProductExists() {
        CustomReporter.log("Open all the products");
        driver.findElement(allProductsSelector).click();

        CustomReporter.log("Search for created product");
        searchForProduct();

        CustomReporter.log("Verify that product is present on the page");
        Assert.assertEquals(driver.findElement(createdProductSelector).getText(),
                    ProductData.getInstance().getName(), "Created product was not found");
        driver.findElement(createdProductSelector).click();

        CustomReporter.log("Verify that product's name matches");
        Assert.assertEquals(driver.findElement(createdProductNameSelector).getText(),
                    ProductData.getInstance().getName().toUpperCase(), "Name of created product does not match");

        CustomReporter.log("Verify that product's price matches");
        Assert.assertEquals(driver.findElement(createdProductPriceSelector).getText().substring(0,
                (driver.findElement(createdProductPriceSelector).getText().length()-2)),
                    ProductData.getInstance().getPrice(), "Price of created product does not match");

        CustomReporter.log("Verify that product's quantity matches");
        Assert.assertEquals(driver.findElement(createdProductQtySelector).getText().substring(0,2),
                    ProductData.getInstance().getQty().toString(), "Quantity of created product does not match");
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
            new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.invisibilityOfElementLocated(ajaxIconSelector));
    }

    /**
     *
     * Method waits for element becomes clickable.
     */
    public void waitForElementClickable(By by) {
            new WebDriverWait(driver, 15)
                    .until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     *
     * Method waits for element becomes clickable.
     */
    public void waitForElementVisible(By by) {
        new WebDriverWait(driver, 15)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    /**
     *
     * Method opens for particular web page.
     * @param url
     */
    public void open(String url) {
            driver.get(url);
    }

    /**
     *
     * Method fills in product data on Admin Product page.
     * @param newProduct
     */
    public void fillInProductData(ProductData newProduct) {
        driver.findElement(productNameInputSelector).sendKeys(newProduct.getName());
        clearCell(productQuantityInputSelector);
        driver.findElement(productQuantityInputSelector).sendKeys(newProduct.getQty().toString());
        clearCell(productPriceInputSelector);
        driver.findElement(productPriceInputSelector).sendKeys(newProduct.getPrice());
        activateProduct();
    }

    /**
     *
     * Method activates a product on product creation page.
     */
    private void activateProduct() {
        driver.findElement(activateSwitchSelector).click();
        waitForElementVisible(successMessageSelector);
    }

    /**
     *
     * Method check that success message appears after saving product.
     */
    public void checkSuccessMessage(){
        waitForElementVisible(successMessageSelector);
        driver.findElement(closeSuccessMessageSelector).click();
    }

    /**
     *
     *@param by
     * Method cleans cell.
     */
    public void clearCell(By by){
        driver.findElement(by).sendKeys(Keys.CONTROL + "a");
        driver.findElement(by).sendKeys((Keys.DELETE));
    }

    /**
     *
     *
     * Method on the main page searches for created product.
     */
    public void searchForProduct(){
        driver.findElement(searchFieldSelector).sendKeys(ProductData.getInstance().getName());
        driver.findElement(searchButtonSelector).click();
    }
}
